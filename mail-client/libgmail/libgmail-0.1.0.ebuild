# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /cvsroot/abeni/abeni/utils.py,v 1.33 2004/08/25 06:35:01 robc Exp $

inherit python

DESCRIPTION="Python bindings to access Google's gmail service"
HOMEPAGE="http://libgmail.sourceforge.net"
SRC_URI="mirror://sourceforge/libgmail/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"

IUSE=""


src_install() {
	python_version
	exeinto /usr/lib/python${PYVER}/site-packages
	doexe libgmail.py constants.py mkconstants.py
	dodir /usr/share/doc/${PF}
	cp -r demos ${D}/usr/share/doc/${PF}
	dodoc ANNOUNCE CHANGELOG README
}
