# Copyright 2005-2006 Radoslaw Stachowiak <rstachowiak@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

ETYPE="sources"
K_WANT_GENPATCHES="base extras"
K_GENPATCHES_VER="7"
inherit kernel-2
detect_version
detect_arch

KEYWORDS="x86"
IUSE="rskerni"
DESCRIPTION="RS kernel sources including the gentoo patchset v$GPV"
SRC_URI="${KERNEL_URI} ${GENPATCHES_URI} ${ARCH_URI}"


#EXTRAVERSION=".$(get_version_component_range 4)-rs"
EXTRAVERSION="-rs"

KV="${OKV}${EXTRAVERSION}"

src_compile() {
	
	cp ${FILESDIR}/${P}_config ${S}/.config || die "can initialize .config"

	# prepare oldconfig
	K_DEFCONFIG=oldconfig
	compile_headers

	# need to save it to survive upgrade (merge -> unmerge)
	cp ${S}/.config ${S}/.config.rs
}

pkg_postinst() {
	#postinst_sources
	kernel-2_pkg_postinst

	# rskerni useflags means build kernel and install
	if use rskerni 
	then
		einfo
		einfo "building kernel $KV_FULL (please wait..)"
		einfo
		cd /usr/src/linux-$KV_FULL || return

		# this cp is needed because at replace (merge/unmerge) its being deleted
		cp .config.rs .config

		# this is stupid patch for gentoo-sources mistake
		# sed -i -e "s:KERN_ERROR:KERN_ERR:" drivers/pcmcia/yenta_socket.c

		unset ARCH
		make || return
		make modules_install || return
		cp System.map /boot/System.map-${KV_FULL}
		cp arch/i386/boot/bzImage /boot/vmlinuz-${KV_FULL}
		cp .config /boot/config-${KV_FULL}
	fi
	einfo
	einfo "Kernel installed, to update modules:"
	einfo 'Please do: emerge -av $(equery b /lib/modules | sed -e "s:^:>=:")'
	einfo
}

pkg_config() {
	einfo "Not used currently."
}

