# Copyright 2005 Radoslaw Stachowiak <radek@alter.pl
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

ETYPE="sources"
inherit eutils kernel-2
detect_version
detect_arch

PROVIDE="virtual/alsa
		 virtual/linux-sources"

IUSE="rspentium2"

# hack to change versions
MY_PR=${PR/r0/r}
MY_PR=${MY_PR/r/}

# MODIFYHERE: extraversion is used in kernel Makefile (results in uname -r)
# set to empty on full releases
KV_EXTRA=".11"
EXTRAVERSION=${KV_EXTRA}-rs${MY_PR}

# okv is simply orginal kernel version
OKV=$KV_MAJOR.$KV_MINOR.$KV_PATCH
# kv_full is used as dir name
KV_FULL=${OKV}${EXTRAVERSION}

S=${WORKDIR}/linux-${KV_FULL}

# MODIFYHERE: version of gentoo patchset
GPV="17-9"
GPV_URL="http://dev.gentoo.org/~dsd/gentoo-sources/tarballs/"
GPV_SRC="${GPV_URL}/genpatches-${KV_MAJOR}.${KV_MINOR}.${GPV}.base.tar.bz2
	     ${GPV_URL}/genpatches-${KV_MAJOR}.${KV_MINOR}.${GPV}.extras.tar.bz2"

# MODIFYHERE: set correct arch
KEYWORDS="x86"

UNIPATCH_LIST="${DISTDIR}/genpatches-${KV_MAJOR}.${KV_MINOR}.${GPV}.base.tar.bz2
	       ${DISTDIR}/genpatches-${KV_MAJOR}.${KV_MINOR}.${GPV}.extras.tar.bz2"
UNIPATCH_DOCS="${WORKDIR}/patches/${KV_MAJOR}.${KV_MINOR}.${KV_PATCH}/0000_README"

DESCRIPTION="RS sources including the gentoo patchset v$GPV"
SRC_URI="${KERNEL_URI} ${GPV_SRC} ${ARCH_URI}"

src_unpack() {

#	echo "OKV: $OKV"
#	echo "KV: $KV"
#	echo "EXTRAVERSION: $EXTRAVERSION"
#	echo "KV_EXTRA: $KV_EXTRA"
#	echo "KV_FULL: $KV_FULL"
#	echo "S: $S"

	if [[ -n "$UNIPATCH_LIST_DEFAULT" ]]
	then
		# this is needed for KV_EXTRA != releases
		ewarn " Skipping patch: $UNIPATCH_LIST_DEFAULT"
		UNIPATCH_LIST_DEFAULT=""
	fi
	kernel-2_src_unpack
}

# this patching is done in compile to avoid lengthy unpack reruns
#
src_compile() {
	
	cp ${FILESDIR}/${PN}-${OKV}_config ${S}/.config || die "can initialize .config"

	if use rspentium2
	then
		epatch ${FILESDIR}/${PN}-2.6.12_config_pentium2.patch
	fi

	# prepare oldconfig
	K_DEFCONFIG=oldconfig
	compile_headers

	# need to save it to survive upgrade (merge -> unmerge)
	cp ${S}/.config ${S}/.config.rs
}

pkg_postinst() {
	postinst_sources

	# symlink means build kernel and install
	if use symlink 
	then
		einfo
		einfo "building kernel $KV_FULL (please wait..)"
		einfo
		cd /usr/src/linux-$KV_FULL || return

		# this cp is needed because at replace (merge/unmerge) its being deleted
		cp .config.rs .config

		# this is stupid patch for gentoo-sources mistake
		# sed -i -e "s:KERN_ERROR:KERN_ERR:" drivers/pcmcia/yenta_socket.c

		unset ARCH
		make || return
		make modules_install || return
		cp System.map /boot/System.map-${KV_FULL}
		cp arch/i386/boot/bzImage /boot/vmlinuz-${KV_FULL}
		cp .config /boot/config-${KV_FULL}
		ewarn
		ewarn "Kernel compiled and installed, please run 'grubi' to put it into grub.conf"
		ewarn 'Please do: emerge -av $(equery b /lib/modules | sed -e "s:^:>=:")'
		ewarn
	fi
}

pkg_postrm() {
	postrm_sources

	# dont need this because we store it in /boot
	rm -f /usr/src/linux-${KV_FULL}.config.gz
}

pkg_config() {
	einfo "Default settings (if not modifed by use flags):"
	einfo " * HIGHMEM is enabled up to 4GB"
	einfo " * 4KSTACKS is enabled"
	einfo " * PREEMPT is NOT enabled (server preemption model)"
	einfo " * SMP is NOT enabled"
	einfo " * PENTIUM3 is default target"
}

