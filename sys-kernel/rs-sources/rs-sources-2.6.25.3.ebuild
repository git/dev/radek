# Copyright 2005-2006 Radoslaw Stachowiak <radek@alter.pl>
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

ETYPE="sources"
inherit eutils kernel-2
detect_version
detect_arch

PROVIDE="virtual/alsa
		 virtual/linux-sources"

IUSE="rspentium2 rskerni"

# hack to change versions
MY_PR=${PR/r0/r}
MY_PR=${MY_PR/r/}

# MODIFYHERE: extraversion is used in kernel Makefile (results in uname -r)
# set to empty on full releases and .digit (e.g. ".6") on sub releases
KV_EXTRA=".3"
# /modifyhere

EXTRAVERSION=${KV_EXTRA}-rs${MY_PR}

# okv is simply orginal kernel version
OKV=$KV_MAJOR.$KV_MINOR.$KV_PATCH
# kv_full is used as dir name
KV_FULL=${OKV}${EXTRAVERSION}

S=${WORKDIR}/linux-${KV_FULL}

# MODIFYHERE: version of gentoo patchset
GPV="25-4"
# /modifyhere
GPV_URL="http://dev.gentoo.org/~dsd/gentoo-sources/tarballs/"
GPV_SRC="${GPV_URL}/genpatches-${KV_MAJOR}.${KV_MINOR}.${GPV}.base.tar.bz2
	     ${GPV_URL}/genpatches-${KV_MAJOR}.${KV_MINOR}.${GPV}.extras.tar.bz2"

# MODIFYHERE: Version of my patchset
RSV="23-1"
# /modifyhere
RSV_URL="http://dev.gentoo.org/~radek/kernel"
RSV_SRC="${RSV_URL}/rspatches-${KV_MAJOR}.${KV_MINOR}.${RSV}.tar.bz2"

# MODIFYHERE: set correct arch
KEYWORDS="x86"
# /modifyhere

UNIPATCH_LIST="${DISTDIR}/genpatches-${KV_MAJOR}.${KV_MINOR}.${GPV}.base.tar.bz2
	       ${DISTDIR}/genpatches-${KV_MAJOR}.${KV_MINOR}.${GPV}.extras.tar.bz2"
# DISBLED		   ${DISTDIR}/rspatches-${KV_MAJOR}.${KV_MINOR}.${RSV}.tar.bz2"

UNIPATCH_DOCS="${WORKDIR}/patches/${KV_MAJOR}.${KV_MINOR}.${KV_PATCH}/0000_README"

DESCRIPTION="RS kernel sources including the gentoo patchset v$GPV"
SRC_URI="${KERNEL_URI} ${GPV_SRC} ${RSV_SRC} ${ARCH_URI}"

src_unpack() {

	if [[ -n "$UNIPATCH_LIST_DEFAULT" ]]
	then
		# this is needed for KV_EXTRA != releases
		ewarn " Skipping patch: $UNIPATCH_LIST_DEFAULT"
		UNIPATCH_LIST_DEFAULT=""
	fi
	kernel-2_src_unpack
}

# this patching is done in compile stage to avoid lengthy unpack reruns on
# potential errors during patching phase
#
src_compile() {
	
	cp ${FILESDIR}/${PN}-${OKV}_config ${S}/.config || die "can initialize .config"

	if use rspentium2
	then
		epatch ${FILESDIR}/${PN}-2.6.12_config_pentium2.patch
	fi

	# prepare oldconfig
	K_DEFCONFIG=oldconfig
	compile_headers

	# need to save it to survive upgrade (merge -> unmerge)
	cp ${S}/.config ${S}/.config.rs
}

pkg_postinst() {
	postinst_sources

	# rskerni useflags means build kernel and install
	if use rskerni 
	then
		einfo
		einfo "building kernel $KV_FULL (please wait..)"
		einfo
		cd /usr/src/linux-$KV_FULL || return

		# this cp is needed because at replace (merge/unmerge) its being deleted
		cp .config.rs .config

		# this is stupid patch for gentoo-sources mistake
		# sed -i -e "s:KERN_ERROR:KERN_ERR:" drivers/pcmcia/yenta_socket.c

		unset ARCH
		make || return
		make modules_install || return
		cp System.map /boot/System.map-${KV_FULL}
		cp arch/i386/boot/bzImage /boot/vmlinuz-${KV_FULL}
		cp .config /boot/config-${KV_FULL}
	fi
	ewarn
	ewarn "Reiser 4 is disabled (not present et all) starting from rs-sources-2.6.23 (radek@20071111)"
	ewarn
	einfo
	einfo "Kernel installed, to update modules:"
	einfo 'Please do: emerge -av $(equery b /lib/modules | sed -e "s:^:>=:")'
	einfo
}

pkg_config() {
	einfo "Not used currently."
}

