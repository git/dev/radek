# Copyright (c) 2004.2005 Radoslaw Stachowiak <radek@alter.pl>
# $Id: grukeri.ebuild 329 2006-12-27 22:01:00Z radek $

DESCRIPTION="My <radek@alter.pl> set of utilities for better linux usage."
HOMEPAGE="http://dev.gentoo.org/~radek/grukeri/"
SRC_URI="${HOMEPAGE}/files/${P}.tar.gz"

LICENSE="GPL2"
SLOT="0"
KEYWORDS="x86"
IUSE=""

RESTRICT="primaryuri"

DEPEND=">=app-admin/syslog-ng-1.6.0
		>=app-shells/bash-3.0
		=app-crypt/gnupg-1*
		app-admin/tmpwatch
		>=sys-apps/cpipe-3.0.0"

S=${WORKDIR}

src_install() {
	
	exeinto /usr/bin
	doexe mailveri
	doexe errpoti
	doexe fixi

	exeinto /usr/sbin
	doexe backupi
	doexe invalidi
	doexe grubi
	doexe kerni
	doexe varlogi
	doexe entepi

	doconfd grukeri
	dodoc *.txt ChangeLog

	exeinto /etc/cron.daily
	doexe grukeri.cron

	insinto /usr/share/doc/${PF}
	doins gpgadmin.asc
}

pkg_postinst() {

	einfo ""
	einfo "You can use grukeri for:"
	einfo ".. mailveri - mail verificator."
	einfo ".. errpoti  - cron error output handler/scheduler"
	einfo ".. fixi     - mp3 renamer + id3tag processor (tobedone)"
	einfo ".. backupi  - universal backup script"
	einfo ".. invalidi - syslog-ng python handler to block failed login attempts"
	einfo ".. grubi    - grub.conf file handler (works together with kerni"
	einfo ".. kerni    - kernel install support"
	einfo ".. varlogi  - /var/log cleaner (removes old logfiles)"
	einfo ".. entepi   - NTP time synchronizer (instead of running ntpd)"
	einfo ""
	ewarn "Do not forget to import gpgkey as a root/backup operator:"
	ewarn "    gpg --import /usr/share/doc/${PF}/gpgadmin.asc"
	ewarn ""
	ewarn "For PostgreSQL/MySQL backups, be sure that *dump commands work without password"
	ewarn "PostgreSQL: echo 'localhost:5432:*:postgres:YOURPASSWORD' >> /root/.pgpass"
	ewarn "MySQL: echo -e '[client]\Npassword=YOURPASSWORD' >> /root/.my.cnf"
	ewarn ""
}
