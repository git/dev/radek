# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="My <radek@alter.pl> set of utilities for better linux usage."
HOMEPAGE="http://dev.gentoo.org/~radek/grukeri/"
SRC_URI="${HOMEPAGE}/files/${P}.tar.gz"

LICENSE="GPL2"
SLOT="0"
KEYWORDS="x86"
IUSE=""

RESTRICT="primaryuri"

DEPEND=">=app-shells/bash-3.0
		=app-crypt/gnupg-1*
		>=sys-apps/cpipe-3.0.0"

S=${WORKDIR}

src_install() {
	
	exeinto /usr/bin
	doexe mailveri
	doexe errpoti
	doexe fixi

	exeinto /usr/sbin
	doexe backupi
	doexe invalidi
	doexe grubi
	doexe kerni

	doconfd grukeri
	dodoc *.txt ChangeLog

	exeinto /etc/cron.daily
	doexe grukeri.cron

	insinto /usr/share/doc/${PF}
	doins gpgadmin.asc
}

pkg_postinst() {

	einfo ""
	einfo "You can use grukeri for:"
	einfo ".. mailveri - mail verificator."
	einfo ".. errpoti  - cron error output handler/scheduler"
	einfo ".. fixi     - mp3 renamer + id3tag processor (tobedone)"
	einfo ".. backupi  - universal backup script"
	einfo ".. invalidi - syslog-ng python handler to block failed login attempts"
	einfo ".. grubi    - grub.conf file handler (works together with kerni"
	einfo ".. kerni    - kernel install support"
	einfo ""
	ewarn "Do not forget to import gpgkey as a root/backup operator:"
	ewarn "    gpg --import /usr/share/doc/${PF}/gpgadmin.asc"
	ewarn ""
	ewarn "For PostgreSQL backup, be sure that pg_dumpall works without password"
	ewarn "Best way is: echo 'localhost:5432:*:postgres:YOURPASSWORD' >> /root/.pgpass"
	ewarn ""
}
