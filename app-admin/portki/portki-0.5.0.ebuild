# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Portage synchronization tool for managing group of gentoo servers."
HOMEPAGE="http://dev.gentoo.org/~radek/portki/"
SRC_URI="${HOMEPAGE}/files/${P}.tar.gz"
LICENSE="GPL2"

SLOT="0"
IUSE=""

KEYWORDS="x86"

DEPEND=">=app-shells/bash-2.0"

RDEPEND="
	>=sys-apps/portage-2.0.51
	>=app-shells/bash-2.0
	>=sys-apps/tmpreaper-1
	>=app-portage/esearch-0.5
	>=app-portage/genlop-0.20
	>=app-portage/gentoolkit"

S=${WORKDIR}

src_install() {
	
	# this directories are installed with normal perms
	dodir /etc/conf.d
	dodir /etc/config-archive

	dodir /usr/bin
	dodir /usr/lib/${PN}

	insinto /usr/lib/${PN}
	exeinto /usr/lib/${PN}

	doins ${PN}.conf
	doexe ${PN}.cron
	doins ${PN}.description.txt
	doins ${PN}.stage.*

	doins dispatch-conf-digest.patch
	doins dispatch-conf.conf-addon

	doins make.conf.recommended

	cp /usr/lib/portage/bin/dispatch-conf .
	patch dispatch-conf dispatch-conf-digest.patch
	newexe dispatch-conf ${PN}.dispatch-conf

	doexe ${PN}.sh
	dosym ${D}/usr/lib/${PN}/${PN}.sh /usr/bin/${PN}

	# now restricted perms perms
	exeopts -m750
	insopts -m640
	diropts -m2750

	dodir /var/state/${PN}

	keepdir /var/state/${PN}/last
	keepdir /var/state/${PN}/put
	keepdir /var/state/${PN}/tmp
	keepdir /var/state/${PN}/update

	insinto /var/state/${PN}
	doins cmd.local.example
	newins cmd.remote.example cmd.remote

	dodir /var/log/${PN}
	
	# configuration inject
	grep -B4000 '#################################################################' ${D}/usr/lib/${PN}/${PN}.conf >> ${D}/etc/conf.d/${PN}
}

pkg_postinst() {

	# chown to portage group have to be in postinst
	chown root:portage -R ${ROOT}/usr/lib/${PN}
	chown root:portage -R ${ROOT}/var/state/${PN}
	chown root:portage -R ${ROOT}/var/log/${PN}

	rm -f /var/state/${PN}/tmp/* 2> /dev/null
	rm -f /var/state/${PN}/last/* 2> /dev/null

	einfo ""
	einfo "Dont forget to copy /usr/lib/${PN}/${PN}.cron to /etc/cron.hourly/"
	einfo "Main configuration file is located: /usr/lib/${PN}/${PN}.conf"
	einfo "But please modify ONLY your local version at: /etc/conf.d/${PN}"
	einfo "You can use ebuild config command (show below at ++) to: "
	einfo ".. create /var/log/portage dir"
	einfo ".. append portki defaults to /etc/make.conf and /etc/dispatch-conf.conf"
	einfo ".. create cron runner at /etc/cron.hourly/portki.cron"
	einfo ""
	einfo "++ ebuild /var/db/pkg/${CATEGORY}/${PF}/${PF}.ebuild config"
	einfo ""
}

pkg_config() {

	rm -fr /etc/portage/portki.* /etc/portage/update
	rm -f /var/log/portki_*
	rm -f /etc/cron.daily/eupdatedb.cron
	rm -f /etc/cron.hourly/portki.cron.hourly
	einfo "Removed old portki files from /etc/portage/portki and /var/log/portki_"

	mkdir /var/log/portage 2>/dev/null
	chmod 750 /var/log/portage
	chown root:portage /var/log/portage
	einfo "Created /var/log/portage (used inf /etc/make.conf as portage logdir)"

	cp -f /usr/lib/${PN}/${PN}.cron /etc/cron.hourly/
	einfo "Installed cron runner at: /etc/cron.hourly/${PN}.cron"

	cat /usr/lib/${PN}/dispatch-conf.conf-addon >> /etc/dispatch-conf.conf
	ewarn "Modified dispatch config file: /etc/dispatch-conf.conf -> INSPECT IT!"

	cat /usr/lib/${PN}/make.conf.recommended >> /etc/make.conf
	ewarn "Modified make.conf file at /etc/make.conf -> INSPECT IT!"

	ewarn "Please also create appropiate /usr/local/XXX-portage/ directories."
	ewarn "Please also remove (or at least inspect) old content of /usr/local/portage/"

}
