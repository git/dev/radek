# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Portage synchronization tool for managing group of gentoo servers."
HOMEPAGE="http://dev.gentoo.org/~radek/portki/"
SRC_URI="${HOMEPAGE}/files/${P}.tar.gz"
LICENSE="GPL2"

SLOT="0"
IUSE=""

KEYWORDS="x86"

DEPEND=">=app-shells/bash-3.0
	>=sys-apps/tmpreaper-1"

RDEPEND="
	>=sys-apps/portage-2.0.51
	>=app-shells/bash-3.0
	>=sys-apps/tmpreaper-1
	>=app-portage/genlop-0.30
	>=app-portage/gentoolkit"

S=${WORKDIR}

src_install() {
	
	# this directories are installed with normal perms
	dodir /etc/conf.d
	dodir /etc/config-archive

	dodir /usr/bin
	dodir /usr/lib/${PN}

	insinto /usr/lib/${PN}
	exeinto /usr/lib/${PN}

	doins ${PN}.conf
	doexe ${PN}.cron
	doins ${PN}.stage.*
	doins TODO.txt

	doins dispatch-conf-digest.patch
	doins dispatch-conf.conf.recommended

	doins make.conf.recommended

	cp /usr/lib/portage/bin/dispatch-conf .
	patch dispatch-conf dispatch-conf-digest.patch
	newexe dispatch-conf ${PN}.dispatch-conf

	doexe ${PN}.sh
	dosym ${D}/usr/lib/${PN}/${PN}.sh /usr/bin/${PN}

	# now restricted perms perms
	diropts -m2751
	dodir /var/state/${PN}

	exeopts -m750
	insopts -m640

	dodir /var/state/${PN}/last
	keepdir /var/state/${PN}/last

	diropts -m2750
	dodir /var/state/${PN}/put
	keepdir /var/state/${PN}/put
	dodir /var/state/${PN}/tmp
	keepdir /var/state/${PN}/tmp
	dodir /var/state/${PN}/update
	keepdir /var/state/${PN}/update

	insinto /var/state/${PN}
	doins cmd.local.example
	newins cmd.remote.example cmd.remote

	dodir /var/log/${PN}
	
	# configuration inject
	grep -B4000 '#################################################################' ${D}/usr/lib/${PN}/${PN}.conf >> ${D}/etc/conf.d/${PN}
}

pkg_postinst() {

	# chown to portage group have to be in postinst
	chown root:portage -R ${ROOT}/usr/lib/${PN}
	chown root:portage -R ${ROOT}/var/state/${PN}
	chown root:portage -R ${ROOT}/var/log/${PN}

	# ugly hack to overcome permissions of already existing dir
	chmod 2751 /usr/lib/${PN} /var/state/${PN} /var/state/${PN}/last

	# now we erase only files not modified in last 30 days
	tmpreaper --mtime 30d /var/state/${PN}/last /var/state/${PN}/tmp >/dev/null 2>/dev/null

	# also this is removed, to allow execution on next run
	rm -f /var/state/${PN}/last/portki.last.cmdr 2> /dev/null

	# patches glsa-check, this can be safely ignored
	patch /usr/bin/glsa-check ${S}/glsa-check_errlevel.patch >/dev/null
	
	einfo ""
	einfo "Dont forget to copy /usr/lib/${PN}/${PN}.cron to /etc/cron.hourly/"
	einfo "Main configuration file is located: /usr/lib/${PN}/${PN}.conf"
	einfo "But please modify ONLY your local version at: /etc/conf.d/${PN}"
	einfo "You can use ebuild config command (show below at ++) to: "
	einfo ".. create /var/log/portage dir"
	einfo ".. append portki defaults to /etc/make.conf and /etc/dispatch-conf.conf"
	einfo ".. create cron runner at /etc/cron.hourly/portki.cron"
	einfo ""
	einfo "++ ebuild /var/db/pkg/${CATEGORY}/${PF}/${PF}.ebuild config"
	einfo ""
}

pkg_config() {

	rm -fr /etc/portage/portki.* /etc/portage/update
	rm -f /var/log/portki_*
	rm -f /etc/cron.daily/eupdatedb.cron
	rm -f /etc/cron.hourly/portki.cron.hourly
	einfo "Removed old portki files from /etc/portage/portki and /var/log/portki_"

	mkdir /var/log/portage 2>/dev/null
	chmod 750 /var/log/portage
	chown root:portage /var/log/portage
	einfo "Created /var/log/portage (used inf /etc/make.conf as portage logdir)"

	cp -f /usr/lib/${PN}/${PN}.cron /etc/cron.hourly/
	einfo "Installed cron runner at: /etc/cron.hourly/${PN}.cron"

	cat /usr/lib/${PN}/dispatch-conf.conf.recommended >> /etc/dispatch-conf.conf
	ewarn "Modified dispatch config file: /etc/dispatch-conf.conf -> INSPECT IT!"

	echo "~app-portage/gentoolkit-0.2.0_pre10 " >> /etc/portage/package.keywords
	echo "~app-portage/genlop-0.30.2" >> /etc/portage/package.keywords
	einfo "Added gentoolkit and genlop needed versions to /etc/portage/package.keywords"

	cat /usr/lib/${PN}/make.conf.recommended >> /etc/make.conf
	ewarn "Modified make.conf file at /etc/make.conf -> INSPECT IT!"

	ewarn "Please also create appropiate /usr/local/XXX-portage/ directories."
	ewarn "Please also remove (or at least inspect) old content of /usr/local/portage/"

}
