# Copyright 2004 Ralf Holzer (rholzer@cmu.edu)
# Distributed under the terms of the GNU General Public License v2


DESCRIPTION="Cryptsetup utility for dm-crypt"
SRC_URI="http://www.saout.de/misc/dm-crypt/${P}.tar.bz2"
HOMEPAGE="http://www.saout.de/misc/dm-crypt/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86"

DEPEND="virtual/glibc
	>=sys-libs/device-mapper-1.00.08
	>=dev-libs/libgcrypt-1.1.92"

src_unpack() {
	unpack ${P}.tar.bz2
	cd ${S}
}

src_compile() {
	econf || die
	emake || die
}

src_install() {
	einstall \
		install \
		|| die

}
