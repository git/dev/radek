# Copyright 2003 Radoslaw Stachowiak
# Distributed under the terms of the GNU General Public License, v2
# $Header: /home/cvsroot/gentoo-x86/app-portage/gentoolkit/files/scripts/mkebuild,v 1.1 2003/08/15 12:23:44 lanius Exp $

DESCRIPTION="Handy calc is a small commandline calculator similar to GNU bc."
SRC_URI="http://www.ne.jp/asahi/linux/timecop/software/${P}.tar.gz"
HOMEPAGE="http://www.ne.jp/asahi/linux/timecop/"
LICENSE="GPL"
SLOT="0"
KEYWORDS="x86"
IUSE=""

DEPEND="yacc"

#RDEPEND=""

S=${WORKDIR}/${P}

src_unpack() {

	unpack ${A}
	cd ${S}

}

src_compile() {

	emake || die
}

src_install () {

	mkdir -p ${D}/usr/bin
    install bc ${D}/usr/bin/hc || die
}



