# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

inherit distutils

RESTRICT="primaryuri"

DESCRIPTION="Python Linux kernel inotify extension"
HOMEPAGE="http://pyinotify.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

IUSE="doc"
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="x86"

PYTHON_MODNAME="pyinotify"

DOCS="README TODO AUTHORS NEWS ChangeLog PKG-INFO"

src_install() {
	distutils_src_install

	if use doc; then

		insinto /usr/share/doc/${P}
		# api documentation
		doins -r doc
		# www page
		doins -r www

		insinto /usr/share/${PN}
		doins -r src/examples
		doins -r src/tests
	fi
}

