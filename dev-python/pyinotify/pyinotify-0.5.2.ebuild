# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

inherit distutils

RESTRICT="primaryuri"

DESCRIPTION="Python Linux kernel inotify extension"
HOMEPAGE="http://pyinotify.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

IUSE=""
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~x86"

PYTHON_MODNAME="pyinotify"

