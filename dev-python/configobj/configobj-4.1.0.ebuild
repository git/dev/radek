# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

inherit distutils

DESCRIPTION="Python ConfigObj extended ini style config file manipulation library"
HOMEPAGE="http://configobj.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.zip"

IUSE="doc"
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="x86"

PYTHON_MODNAME="configobj"

src_install() {
		distutils_src_install

		if use doc; then
			dodoc docs/validate.txt
			dodoc docs/configobj.txt
		fi
}
