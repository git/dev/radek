# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: Exp $

inherit distutils

# cause its not on gentoo mirrors
RESTRICT="primaryuri"

DESCRIPTION="Python implementation of SPF (Sender Permitted From)"
HOMEPAGE="http://www.wayforward.net/spf/"
SRC_URI="${HOMEPAGE}/${P}.tar.gz"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="x86"

DEPEND=""

RDEPEND=">=dev-python/pydns-2.3.0"

PYTHON_MODNAME="spf"

DOCS="README CHANGELOG PKG-INFO"

