# Copyright 1999-2005 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit kde

RESTRICT="primaryuri"

need-kde 3

DESCRIPTION="small kicker applet for KDE 3.x which shows the current CPU temperature and frequency."
SRC_URI="http://www.kde-apps.org/content/files/33257-${P}.tar.gz"
HOMEPAGE="http://kde-apps.org/content/show.php?content=33257"

IUSE="arts"
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"

S=${WORKDIR}/${PN}

src_compile() {
	use !arts && arts="--without-arts"
	./configure --prefix=$(kde-config --prefix) ${arts} || die "./configure failed"
	emake || die "emake failed"
}
