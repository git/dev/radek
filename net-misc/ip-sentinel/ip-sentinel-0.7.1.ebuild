# Copyright 1999-2003 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2

DESCRIPTION="Keeps Your IP-space clear"
HOMEPAGE="http://www-user.tu-chemnitz.de/~ensc/ip-sentinel/"
SRC_URI="http://www-user.tu-chemnitz.de/~ensc/ip-sentinel/files/${P/_/}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86"

IUSE=""

DEPEND=""
RDEPEND=""

src_install() {
	cd $S
	dosbin src/ip-sentinel
	doman doc/ip-sentinel.8
}
