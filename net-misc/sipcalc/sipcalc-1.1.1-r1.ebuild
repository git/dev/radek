# Copyright 1999-2002 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: /home/cvsroot/gentoo-x86/net-misc/sipcalc/sipcalc-1.1.1-r1.ebuild,v 1.1 2002/11/26 22:06:11 verwilst Exp $

DESCRIPTION="IPv4 and IPv6 subnet calculations"
SRC_URI="http://www.routemeister.net/projects/sipcalc/files/sipcalc-1.1.1.tar.gz"
HOMEPAGE="http://www.routemeister.net/projects/sipcalc/"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="x86"
DEPEND=""

S=${WORKDIR}/sipcalc-1.1.1


src_compile() {
	
    myconf="$myconf --prefix=/usr"
    
    ./configure $myconf || die
    emake || die

}

src_install() {

	make DESTDIR=${D} install || die

}
