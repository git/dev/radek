# Copyright 1999-2003 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2

DESCRIPTION="Copy its stdin to stdout while measuring time it takes to read and write."
SRC_URI="http://download.berlios.de/cpipe/${P}.tar.gz"
HOMEPAGE="http://cpipe.berlios.de/"
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="x86"

DEPEND=""

src_unpack() {
	unpack ${A}
	cd ${S}
	sed -i -e "s:-O2 -W -Wall -pedantic:${CFLAGS}:" \
		makefile* || die "cleaning up makefiles"
}


src_compile() {
	cd ${S}
	touch cmdline.c cmdline.h cpipe.1
	emake CC=gcc || die
}

src_install() {
	doman cpipe.1
	dodoc CHANGES
	exeinto /usr/bin
	doexe cpipe
}
