# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="External policy checker for the postfix mail server (greylister + SPF)"
HOMEPAGE="http://www.tummy.com/Community/software/tumgreyspf/"
SRC_URI="ftp://ftp.tummy.com/pub/tummy/${PN}/${P}.tar.gz"
LICENSE="GPL-2"

SLOT="0"
IUSE=""

RESTRICT="primaryuri"

KEYWORDS="x86"

DEPEND=""

RDEPEND="
	>=dev-lang/python-2.4
	>=dev-python/pyspf-1.6"

src_unpack() {
		unpack ${A}
		cd "${S}"
		#-e "/^CXX=/s:g++:$(tc-getCXX):" \
		sed -i \
			-e 's:^sys.path.append.*:sys.path.append("/usr/lib/tumgreyspf"):' \
			-e 's:^defaultConfigFilename.*:defaultConfigFilename = "/etc/tumgreyspf.conf":' \
			tumgreyspf tumgreyspf-* tumgreyspf*.py || die "changing hardcoded directory paths"

		sed -i \
			-e 's|^configPath.*|configPath="file:///var/lib/tumgreyspf/config"|' \
			-e 's|^greylistDir.*|greylistDir="/var/lib/tumgreyspf/data"|' \
			tumgreyspf.conf || die "changing hardcoded directory paths"
}

src_install() {
	
	# this directories are installed with normal perms
	dodir /usr/bin
	dodir /usr/lib/${PN}

	insinto /usr/lib/${PN}
	exeinto /usr/lib/${PN}

	doins ${PN}-test.conf
	doins ${PN}supp.py

	doexe ${PN}-stat
	doexe ${PN}-clean
	doexe ${PN}-configtest
	doexe ${PN}

	dosym ${D}/usr/lib/${PN}/${PN} /usr/sbin/${PN}
	dosym ${D}/usr/lib/${PN}/${PN}-clean /usr/sbin/${PN}-clean
	dosym ${D}/usr/lib/${PN}/${PN}-stat /usr/sbin/${PN}-stat
	dosym ${D}/usr/lib/${PN}/${PN}-configtest /usr/sbin/${PN}-configtest

	dodoc TODO WHATSNEW README README.QuickStart README.performance

	dodir /etc
	insinto /etc
	doins ${PN}.conf

	dodir /etc/cron.daily
	echo -e "#!/bin/sh\n/usr/sbin/${PN}-clean" >> ${D}/etc/cron.daily/${PN}.cron
	fperms 755 /etc/cron.daily/${PN}.cron

	dodir /var/lib/${PN}
	dodir /var/lib/${PN}/config
	insinto /var/lib/${PN}/config
	newins __default__.dist __default__

	diropts -m750
	insopts -m640
	dodir /var/lib/${PN}/data
	dodir /var/lib/${PN}/blackhole
	fowners /var/lib/${PN}/data
	fowners /var/lib/${PN}/blackhole

	keepdir /var/lib/${PN}/data
	keepdir /var/lib/${PN}/blackhole
}

pkg_postinst() {
	einfo ""
	einfo "Configuration file is located: /etc/${PN}.conf"
	einfo "File system configuration is in: /var/lib/${PN}/config"
	einfo "Cron file was installed under: /etc/cron.daily/${PN}.cron"
	einfo ""
	einfo "Do not forget to modify postfix (master.cf and main.cf)"
	einfo "You can find more detailed instruction in /usr/share/doc/${P}"
	einfo ""
}

